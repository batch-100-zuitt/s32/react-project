const coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "is a programming language",
		price: 45000,
		onOffer: true,
		start_date:"2021-02-20",
		end_date: "2021-08-19"
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "is a programming language",
		price: 50000,
		onOffer: true,
		start_date:"2021-06-19",
		end_date: "2021-12-18"
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "this is a programming language",
		price: 55000,
		onOffer: true,
		start_date:"2021-07-20",
		end_date: "2021-06-19"
	}


]

export default coursesData;