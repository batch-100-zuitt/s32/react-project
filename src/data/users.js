const usersData = [
	{
		email: "admin@gmail.com",
		password: "admin123",
		isAdmin: true

	},
	{
		email: "juan@gmail.com",
		password: "juan123",
		isAdmin: false
		
	}
]

export default usersData;