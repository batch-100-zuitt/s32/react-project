import React, { Fragment, useState } from 'react';
import Navbar from './components/Navbar'; 
import Container from 'react-bootstrap/Container'

import {	BrowserRouter as Router } from 'react-router-dom';
import {	Route, Switch	} from 'react-router-dom';
import { UserProvider } from './UserContext';



import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';




function App() {
	//State Hook for the user state that's defined here for a global scope
	//Initialised as an object with properties from the localstorage
	const [user, setUser] = useState({
		email: localStorage.getItem('email'),

    //Data stored in localStorage is converted into string. 
      isAdmin: localStorage.getItem('isAdmin') === 'true'



	})
	//Function for clearing LocalStorage on logout
	const unsetUser = () => {
		localStorage.clear();

    //changed the value of the user state back to it's original value
    setUser({
        email: null,
        isAdmin: null
    });
	}
  
   return (
      <Fragment>
      	<UserProvider value= {{user, setUser, unsetUser}}>
       		<Router>
        	<Navbar />
        	<Container>
        		  <Switch>
        			 <Route exact path="/" component={Home}/>
					     <Route exact path="/Courses" component={Courses}/>
					     <Route exact path="/login" component={Login}/>
					     <Route exact path="/register" component={Register}/>
               <Route component= {Error} />
        		  </Switch>	
       	    </Container>
        	</Router>
         </UserProvider>
      </Fragment>
       
  );
}

export default App;                                             
                                                                                