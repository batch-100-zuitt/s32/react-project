import React, {	Fragment } from 'react';

import Course from '../components/Course';
import coursesData from '../data/courses';


export default function Courses(){
	const courses = coursesData.map((course) => {
		return
		(
			<Course key={course.id} course={course} />
		);
	});

		return
		(
			<Fragment>
				{courses}
			</Fragment>
		)
		
}

