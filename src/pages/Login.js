import React, {useState,useEffect, useContext} from 'react';

import {Form,Button,Container} from 'react-bootstrap';
import UserContext from '../UserContext';







export default function Login() {

	const { user, setUser} = useContext(UserContext);
	const	[email,setEmail] = useState("");
	const	[password,setPassword] = useState("");
	const	[isDisabled,setIsDisabled] = useState(true);


	function login(e) {

		e.preventDefault();

		setEmail('');
		setPassword('');
	

		alert("Log In Successful")
	}

	useEffect(() => {

		let isEmailNotEmpty = email !== '';
 		let isPasswordNotEmpty = password !== '';

 		if( isEmailNotEmpty &&
 			isPasswordNotEmpty 
 			)
 		{
 			setIsDisabled(false);
 		}else{
 			setIsDisabled(true);
 		}

	}, [email,password]);



	return(

		<Container>
			<Form onSubmit={(e) => login(e)}>
				<h3>LogIn</h3>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control type="email"
					placeholder="Input Email"
					value=	{email}
					onChange={(e) =>setEmail(e.target.value)}
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password"
					placeholder="Input password"
					value = {password}
					onChange={(e) =>setPassword(e.target.value)}
					/>
				</Form.Group>

				
				
				<Button variant="primary" type="submit" disabled={isDisabled}>
				Log in
				</Button>

			</Form>
		</Container>

	)
}