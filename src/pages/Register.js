import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';

export default function Register(){
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);


	function register(e){
		e.preventDefault();

		setEmail('');
		setPassword1('');
		setPassword2('');

		console.log('Thank you for registering!');
	}

	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '') && (password2 === password1)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[email,password1,password2])

	return(
		<Form onSubmit={(e) => register(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
					/>
				<Form.Text>Text className</Form.Text>
			</Form.Group>

			<Form.Group controlId="userPassword">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
					/>
			</Form.Group>

			<Form.Group controlId="userPassword2">
				<Form.Label>Password2</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Confirm Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
					/>
			</Form.Group>

			{isActive?
				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>Not available</Button>
			}
		</Form>
	)
}
