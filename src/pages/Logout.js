import React, { useContext,useEffect }from 'react';
import {Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	const { unsetUser, setUser} = useContext(UserContext);

	//clear the local storage of the user's information
	unsetUser();

	//Set the user state back to it's original value
	setUser({email.null});
	
	//invoke unset User only after initial render
	useEffect(() => {
		unsetUser();
	})

	//Redirect back to login
	return (
		<Redirect to='/login' />

	)
}