import React from 'react';
import { Row, Col, Card} from 'react-bootstrap';


export default function Highlights(){
	return(
		<Row>
			<Col xs={12} md={4}>
				<Card className = "cardHighlight">
					<Card.Body>
						<Card.Title>Learn From Home</Card.Title>
						<Card.Text>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className = "cardHighlight">
					<Card.Body>
						<Card.Title>Study Now Pay Later</Card.Title>
						<Card.Text>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
						</Card.Text>
						</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className = "cardHighlight">
					<Card.Body>
						<Card.Title>Come and join us</Card.Title>
						<Card.Text>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>






	)

}